#include <GL/glew.h>//before including sdl and opengl
#include <SDL.h>
#include <SDL_opengl.h>

#include <iostream>
#include <fstream>
#include <string>

GLuint LoadShader(const char* filename, GLenum type);

int wmain(int argc, char *argv[])
{
	//Initialize all the things
	SDL_Init(SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);

	SDL_Window* window = SDL_CreateWindow("OpenGL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_OPENGL);// | SDL_WINDOW_FULLSCREEN);

	SDL_GLContext context = SDL_GL_CreateContext(window); //OpenGl context creation

	//GL wrangler stuff
	glewExperimental = GL_TRUE;
	glewInit();
	GLenum glErr = glGetError(); //Suppress GLEW init ENUM_ERROR

	//----------------------------------------
	//Experiment Begin
	//----------------------------------------

	//Create ShaderProgram
	GLuint vertexShader = LoadShader("Resources/VertexShader.glsl", GL_VERTEX_SHADER);
	GLuint geometryShader = LoadShader("Resources/GeometryShader.glsl", GL_GEOMETRY_SHADER);
	GLuint fragmentShader = LoadShader("Resources/FragmentShader.glsl", GL_FRAGMENT_SHADER);

	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, geometryShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	glUseProgram(shaderProgram);

	// Create VAO
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	//Crate VBO
	GLuint vbo;
	glGenBuffers(1, &vbo);

	float points[] = {
		// Coordinates  Color             Sides
		-0.45f,  0.45f, 1.0f, 0.0f, 0.0f,  4.0f,
		 0.45f,  0.45f, 0.0f, 1.0f, 0.0f,  8.0f,
		 0.45f, -0.45f, 0.0f, 0.0f, 1.0f, 16.0f,
		-0.45f, -0.45f, 1.0f, 1.0f, 0.0f, 32.0f
	};

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

	// Specify layout of point data
	GLint posAttrib = glGetAttribLocation(shaderProgram, "pos");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 6 * sizeof(float), 0);
	
	GLint colAttrib = glGetAttribLocation(shaderProgram, "color");
	glEnableVertexAttribArray(colAttrib);
	glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(2 * sizeof(float)));
	
	GLint sidesAttrib = glGetAttribLocation(shaderProgram, "sides");
	glEnableVertexAttribArray(sidesAttrib);
	glVertexAttribPointer(sidesAttrib, 1, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(5 * sizeof(float)));

	SDL_Event windowEvent; //Main Loop
	while (true)
	{
		if (SDL_PollEvent(&windowEvent))
		{
			if (windowEvent.type == SDL_QUIT) break;
			if (windowEvent.type == SDL_KEYUP &&
				windowEvent.key.keysym.sym == SDLK_ESCAPE) break;
		}

		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glDrawArrays(GL_POINTS, 0, 4);

		//Swap front and back buffer
		SDL_GL_SwapWindow(window);
	}

	glDeleteProgram(shaderProgram);
	glDeleteShader(fragmentShader);
	glDeleteShader(vertexShader);
		
	SDL_GL_DeleteContext(context);
	SDL_Quit();
	return 0;
}

GLuint LoadShader(const char* filename, GLenum type)
{
	using namespace std;

	cout << "Compiling Shader: " << filename << " . . . ";

	string shaderSourceStr;

	string extractedLine;
	ifstream shaderFile;
	shaderFile.open(filename);
	if (!shaderFile)
	{
		cout << "  . . . FAILED!" << endl;
		cout << "    Opening shader file failed." << endl;
		return -1;
	}
	while (shaderFile.eof() == false)
	{
		getline(shaderFile, extractedLine);
		shaderSourceStr += extractedLine;
		shaderSourceStr += "\n";
	}
	shaderFile.close();

	const char *shaderSource = shaderSourceStr.c_str();
	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &shaderSource, NULL);

	GLint status;
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (!(status == GL_TRUE))
	{
		char buffer[512];
		glGetShaderInfoLog(shader, 512, NULL, buffer);
		cout << "  . . . FAILED!" << endl;
		cout << "    Compiling shader failed." << endl;
	}
	else
	{
		cout << "  . . . SUCCESS!" << endl;
	}

	return shader;
}