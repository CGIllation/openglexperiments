#include <GL/glew.h>//before including sdl and opengl
#include <SDL.h>
#include <SDL_opengl.h>

#include <iostream>
#include <fstream>
#include <string>

GLuint LoadShader(const char* filename, GLenum type);

int wmain(int argc, char *argv[])
{
	//Initialize all the things
	SDL_Init(SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);

	SDL_Window* window = SDL_CreateWindow("OpenGL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_OPENGL);// | SDL_WINDOW_FULLSCREEN);

	SDL_GLContext context = SDL_GL_CreateContext(window); //OpenGl context creation

	//GL wrangler stuff
	glewExperimental = GL_TRUE;
	glewInit();
	GLenum glErr = glGetError(); //Suppress GLEW init ENUM_ERROR

	//----------------------------------------
	//Experiment Begin
	//----------------------------------------

	//Create ShaderProgram
	GLuint shader = LoadShader("Resources/VertexShader.glsl", GL_VERTEX_SHADER);//Shader
	GLuint geoShader = LoadShader("Resources/GeometryShader.glsl", GL_GEOMETRY_SHADER);

	GLuint program = glCreateProgram();//Program
	glAttachShader(program, geoShader);
	glAttachShader(program, shader);

	const GLchar* feedbackVaryings[] = { "outValue" };//output attributes
	glTransformFeedbackVaryings(program, 1, feedbackVaryings, GL_INTERLEAVED_ATTRIBS);

	glLinkProgram(program);//link
	glUseProgram(program);

	GLuint vao;//vao
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	GLfloat data[] = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f };//vdata

	GLuint vbo;//vbo
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);

	GLint inputAttrib = glGetAttribLocation(program, "inValue");//input layout
	glEnableVertexAttribArray(inputAttrib);
	glVertexAttribPointer(inputAttrib, 1, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint tbo;//output vbo
	glGenBuffers(1, &tbo);
	glBindBuffer(GL_ARRAY_BUFFER, tbo); 
	glBufferData(GL_ARRAY_BUFFER, sizeof(data) * 3, nullptr, GL_STATIC_READ);

	glEnable(GL_RASTERIZER_DISCARD);//disable rasterizer

	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, tbo);//bind output vbo to transform feedback (vertex shader ouput)

	GLuint query;//amount query
	glGenQueries(1, &query);

	glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query); // begin recording amount

	glBeginTransformFeedback(GL_TRIANGLES);//enter transform feedback mode

	glDrawArrays(GL_POINTS, 0, 5);// "draw"

	glEndTransformFeedback(); //duuuuh

	glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN); // stop recording amount

	glFlush(); // flush opengl command buffer

	GLuint primitives;//retrieve amount
	glGetQueryObjectuiv(query, GL_QUERY_RESULT, &primitives);

	std::cout << std::endl << primitives << " primitives written!" << std::endl << std::endl;

	// Fetch and print results
	GLfloat feedback[15];
	glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, sizeof(feedback), feedback);

	for (int i = 0; i < 15; i++) {
		std::cout << feedback[i] << std::endl;
	}

	glDeleteProgram(shader);
	glDeleteShader(program);
		
	SDL_GL_DeleteContext(context);
	SDL_Quit();

	std::cin.get();
	return 0;
}

GLuint LoadShader(const char* filename, GLenum type)
{
	using namespace std;

	cout << "Compiling Shader: " << filename << " . . . ";

	string shaderSourceStr;

	string extractedLine;
	ifstream shaderFile;
	shaderFile.open(filename);
	if (!shaderFile)
	{
		cout << "  . . . FAILED!" << endl;
		cout << "    Opening shader file failed." << endl;
		return -1;
	}
	while (shaderFile.eof() == false)
	{
		getline(shaderFile, extractedLine);
		shaderSourceStr += extractedLine;
		shaderSourceStr += "\n";
	}
	shaderFile.close();

	const char *shaderSource = shaderSourceStr.c_str();
	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &shaderSource, NULL);

	GLint status;
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (!(status == GL_TRUE))
	{
		char buffer[512];
		glGetShaderInfoLog(shader, 512, NULL, buffer);
		cout << "  . . . FAILED!" << endl;
		cout << "    Compiling shader failed." << endl;
	}
	else
	{
		cout << "  . . . SUCCESS!" << endl;
	}

	return shader;
}